import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllTodoComponent } from './pages/all-todo/all-todo.component';
import { ActiveTodoComponent } from './pages/active-todo/active-todo.component';
import { CompletedTodoComponent } from './pages/completed-todo/completed-todo.component';


const routes: Routes = [
  {
    path: '',
    component: AllTodoComponent,
  },
  {
    path: 'active',
    component: ActiveTodoComponent,
  },
  {
    path: 'completed',
    component: CompletedTodoComponent,
  },
  {
    path: '**',
    redirectTo: '',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
