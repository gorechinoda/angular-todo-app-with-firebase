import { Component, OnInit } from '@angular/core';
import { FirebaseGetwayService } from 'src/app/services/firebase-getway.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  todos=0;

  constructor(private db: FirebaseGetwayService) {

  }

  ngOnInit(): void {
    this.db.readTodo().orderByChild("completed").equalTo(true).on('value',(snap)=>{
      this.todos = snap.numChildren();
    });
  }

  clearCompleted(){
    this.db.readTodo().orderByChild("completed").equalTo(true).once("value",(snap)=>{
      snap.forEach(data=>{
        data.ref.remove();
      })
    });
  }

}
