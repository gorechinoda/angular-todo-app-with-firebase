import { Component, OnInit, Input } from '@angular/core';
import { FirebaseGetwayService } from 'src/app/services/firebase-getway.service';
import { FormControl } from '@angular/forms';
import { element } from 'protractor';
import { AngularFireList, SnapshotAction } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() state;

  name = new FormControl('');

  todos;
  constructor(private firebaseGetway: FirebaseGetwayService) {
   }

  ngOnInit(): void {
    console.log(this.state);
    this.firebaseGetway.readTodo().orderByKey().on('value',(value)=>{
      this.todos = [];
      value.forEach(snap=>{
        if(this.state == 0){
          this.todos.push({
            key:snap.key,
            completed:snap.val().completed,
            name:snap.val().name,
          });
        }else if(this.state == 1){
          if(snap.val().completed)
          {
            this.todos.push({
              key:snap.key,
              completed:snap.val().completed,
              name:snap.val().name,
            })
          }
        }else{
          if(!snap.val().completed)
          {
            this.todos.push({
              key:snap.key,
              completed:snap.val().completed,
              name:snap.val().name,
            })
          }
        }
      });
    });
  }

  save(){
    this.firebaseGetway.addTodo(this.name.value);
  }

  update(todo){
    this.firebaseGetway.updateTodo(todo);
  }

  delete(todo){
    this.firebaseGetway.deleteTodo(todo);
  }

}
