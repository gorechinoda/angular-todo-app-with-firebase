import { TestBed } from '@angular/core/testing';

import { FirebaseGetwayService } from './firebase-getway.service';

describe('FirebaseGetwayService', () => {
  let service: FirebaseGetwayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseGetwayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
