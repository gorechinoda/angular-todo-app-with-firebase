import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirebaseGetwayService {

  private todoDb: firebase.database.Reference;

  constructor(private db: AngularFireDatabase) {
    this.db.list("todo").snapshotChanges().toPromise();
    this.todoDb = this.db.database.ref('todo');
  }

  addTodo(value: string): void {
    var todo = {
      name:value,
      completed: false,
    }
    this.db.database.ref("todo").push(todo);
  }

  deleteTodo(todo: any): void {
    this.db.database.ref("todo/"+todo.key).remove();
  }

  updateTodo(todo:any){
    this.db.database.ref("todo/"+todo.key).update({completed:!todo.completed});
  }

  readTodo(): firebase.database.Reference{
    return this.todoDb;
  }

}
